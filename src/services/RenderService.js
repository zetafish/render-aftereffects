const { parse } = require('path');
const { init, render } = require('@nexrender/core');

const getConvertExternalFootageSourceToLocalJsxScript = (aepPath, assets) => `
var dynamicLayers = [${assets.map(asset => `'${asset.layerName}'`).join(',')}];

nexrender.selectCompositionsByName('*', function (composition) {
  for (var i = 1; i <= composition.numLayers; i++) {
    var layer = composition.layer(i);

    // FIXME: better comp filtering instead of layer.source.name.indexOf
    if (layer instanceof AVLayer && dynamicLayers.indexOf(layer.name) === -1 && layer.source.name.indexOf('.') !== -1) {
      nexrender.replaceFootage(layer, '${parse(aepPath).dir.replace(
        /\\/g,
        '/',
      )}/' + layer.source.name);
    }
  }
});
`;

const initRender = settings => (
  aepPath,
  {composition, assets, type},
  cwd,
  output
) => {
  return render(
    {
      template: {
        src: `file://${aepPath}`,
        composition,
        settingsTemplate: type === 'DRAFT' ? 'Draft Settings' : undefined,
      },
      assets: [
        ...assets,
        {
          src: `data:,${encodeURIComponent(
            getConvertExternalFootageSourceToLocalJsxScript(aepPath, assets),
          )}`,
          type: 'script',
        },
      ],
      actions: {
        postrender: [
          {
            module: '@nexrender/action-encode',
            output: output.filename,
            params: output.command
            //preset: 'mp4',
          },
          {
            module: '@nexrender/action-copy',
            input: output.filename,
            output: `${cwd}\\${output.filename}`
        }
        ],
      },
    },
    init({
      logger: console,
      stopOnError: true,
      verbose: true,
      ...settings,
    }),
  );
};

module.exports = {
  initRender,
};
