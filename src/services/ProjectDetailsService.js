const axios = require('axios');

/**
 * Reads the project details
 * 
 * @param {string} renderAPI The url of the render API
 * @param {string} renderAPI The uuid of the project
 */
async function getDetails(renderAPI, correlationId) {

    try {
        const resp = await axios.get(`${renderAPI}/correlations/${correlationId}`);
        return resp.data;
    } catch (err) {
        throw new Error(`Can't read project details: ${err.message}`);
    }
}

/**
 * Sends the generation result back
 * 
 * @param {string} renderAPI The url of the render API
 * @param {string} renderAPI The uuid of the project
 * @param {string} state Status of the generation
 * @param {string} message Contains the finalUrl if success or the error message
 */
async function sendResult(renderAPI, correlationId, state, message, finalVideoDetails) {

    let data = {};
    if (state === 'succeeded') {
        data.url = message;
        data.duration = finalVideoDetails.duration,
            data.width = finalVideoDetails.width,
            data.height = finalVideoDetails.height,
            data.bitrate = finalVideoDetails.bitrate
    } else {
        data.cause = message;
    }

    await axios.post(`${renderAPI}/correlations/${correlationId}/result`, {
        state: state,
        data: data
    })
}

async function reportRunningStatus(renderAPI, correlationId) {

    await axios.post(`${renderAPI}/correlations/${correlationId}/running`)
        .catch(err => {
            if (err.response.status === 400) {
                sendResult(renderAPI, correlationId, 'failed', 'Running endpoint responded with status code 400, rendering process was aborted');
                process.exit(1);
            }
        })
}

module.exports = {
    getDetails,
    sendResult,
    reportRunningStatus
};