//const S3 = require('aws-sdk/clients/s3');
const AWS = require('aws-sdk');
const { mkdtempSync, writeFile, readFileSync, copyFileSync, createReadStream } = require('fs');
const { tmpdir } = require('os');
const { join, basename } = require('path');
const find = require('find');
const { exec } = require('child_process');
const AmazonS3URI = require('amazon-s3-uri');
const { Asset } = require('../models/Asset');

const s3 = new AWS.S3({apiVersion: '2011-06-15'});

//Creating a temp folder where will be the static files stored
const tmpDir = mkdtempSync(join(tmpdir(), 'videofier-'));
console.log(`The generated temp dir: ${tmpDir}`);

const downloadFilesAndInstallFonts = async (baseUrl) => {

  //font regExp
  const fontRegExp = /\.ttf$|\.otf$|\.fon$|\.ttc$/i;

  //Reading the aws Key from baseUrl
  const {region, bucket, key} = await getBaseAwsPathFromBaseUrl(baseUrl);

  //Update the region
  AWS.config.update({region: region});

  //Donwloading files from s3
  await listFiles(key.replace(/\/$/, ''));

  //Installing fonts
  await installFonts();

  //Searching template files in the downloaded folders
  return await findTemplateFile();

  /**
   * 
   * @param {string} baseUrl The aws url where the template and static files are stored
   */
  function getBaseAwsPathFromBaseUrl(baseUrl){
    return new Promise((resolve, reject) => {
      try {
        awsObj = AmazonS3URI(baseUrl);
        resolve(awsObj);
      } catch(err){
        reject(new Error(`${baseUrl} is not a valid S3 url`));
      }
    })
  }

  /**
   * 
   * @param {string} baseAwsPath The base path of the project
   */
  function listFiles(baseAwsPath){
    return new Promise((resolve, reject) => {
      s3.listObjectsV2({Prefix: baseAwsPath, Bucket: bucket}, function(err, data){
        if (err) reject(new Error(`Cannot connect to the S3 bucket: ${bucket}`));
        if(data.Contents.length === 0) reject(new Error(`The base folder: ${baseAwsPath} does not exists.`));
        const promises = data.Contents.map(file => donwloadFile(file, tmpDir));

        //When all promises are arrived resolve
        return Promise.all(promises).then(data => {
          resolve()
        })
      })
    });
  }

  /**
   * 
   * @param {object} file AWS file object
   */
  function donwloadFile(file){
    return new Promise((resolve, reject) => {

      let dest = tmpDir + "\\" + basename(file.Key);
      s3.getObject({Key: file.Key, Bucket: bucket}, (err, data) => {
        if (err) reject(new Error(`Error, during file [${file.Key}] donwload: ${err.message}`));
        writeFile(dest, data.Body, (err) => {
          if (err) reject(new Error(`Error, during file [${file.Key}] save: ${err.message}`));
          resolve();
      });
      })
    })
  }

  /**
   * 
   * Searching for fonts, if any fonts found, intall it
   */
  function installFonts(){
    return new Promise((resolve, reject) => {

      find.file(fontRegExp, tmpDir, files => {
        if(files.length > 0){
          files.forEach(font => {
            copyFileSync(font, basename(font), (error) => {
              reject(new Error(`Error, during font files copy: ${error.message}`));
            });
          });
          exec('FontReg.exe /copy', function(error, stdout, stderr){
            if(error){
              reject(new Error(`Error, during font installation: ${error.message}`));
            }
            resolve();
          });
        }else{
          resolve();
        }
      }).error(error => {
        reject(new Error(`Error, during font search: ${error.message}`));
      });
    })

  }

  /**
   * 
   * @param {string} tmpDir Local dir path where will be the file stored
   */
  function findTemplateFile(){
    return new Promise((resolve, reject) => {

      find.file(/\.aep$/, tmpDir, files => {
        if (!files.length) reject(new Error(`Can't find template (.aep) file`));
        if (files.length > 1) reject(new Error(`There is more than one template (.aep) file in the zip file, please upload a template with one .aep file`));
        resolve(files[0]);
      })
      .error(error => {
        reject(new Error(`Error, during project.aep search: ${error.message}`));
      });

    })
  }

};

/**
 * Reads the composition name from a txt
 */
const readComposition = async () => {
  return new Promise((resolve, reject) => {
    find.file('project.json', tmpDir, files => {
      console.log(files.length)
      if (files.length === 0){
        reject(new Error(`Can't find project.json.`));
      }else{
        let project = JSON.parse(readFileSync(files[0], 'utf8'));
        if(typeof project.composition === 'undefined'){
          reject(new Error(`Can't find master composition in project.json`));
        }
        resolve(project.composition);
      }
    })
    .error(error => {
      reject(new Error(`Error, during project.json search: ${error.message}`));
    });
  })
};

/**
 * Reads the layers from template file
 */
 const readPlaceholdersFromTemplateJson = async (content) => {
  return new Promise((resolve, reject) => {
    find.file('template.json', tmpDir, files => {
      if (!files || files.length === 0 || typeof files === 'undefined'){
        resolve(false);
      }else{
        let template = JSON.parse(readFileSync(files[0], 'utf8'));
        if(typeof template.placeholders === 'undefined'){
          reject(new Error(`Can't find placeholders in template.json`));
        }
  
        let result = {};
        if(typeof template.composition === 'undefined'){
          reject(new Error(`Can't find master composition in template.json`));
        }

        result.composition = template.composition;
        result.assets = [];
        for(let placeholder in template.placeholders){
          if(typeof template.placeholders[placeholder].layers !== 'undefined'){
              const layers = template.placeholders[placeholder].layers;
              layers.forEach(layer => {

                  let assetObj = Asset(content[placeholder], layer, placeholder);
                  if(typeof assetObj !== 'object'){
                    reject(new Error(assetObj));
                  }
                  result.assets.push(assetObj);
              });
          }
        }
        resolve(result);
      }
    })
    .error(error => {
      reject(new Error(`Error happent during template.json search: ${error.message}`));
    });
  })
};

/**
 * 
 * @param {string} bucket Name of the AWS Bucket where the result will be uploaded
 * @param {int} advertiserId Advertiser indentifier
 * @param {string} filename The generated filename
 */
const uploadFinalVideo = async (bucket, correlationId, advertiserId, outputFilename, extension) => {
  return new Promise((resolve, reject) => {

    let baseAwsKey = `${advertiserId}/video-aftereffects/${correlationId}.${extension}`;
    let fileStream = createReadStream(outputFilename);
    fileStream.on('error', function(error) {
      reject(new Error(`Failed to create the filestream: ${error.message}`));
    });

    let params = {Bucket: bucket, Key: baseAwsKey, Body: fileStream};
    s3.upload(params, function(error, data) {
      if (error) {
        reject(new Error(`Failed to upload to S3: ${error.message}`));
      } if (data) {
        resolve(data.Location);
      }
    });
  })
};


module.exports = {
  downloadFilesAndInstallFonts,
  readComposition,
  uploadFinalVideo,
  readPlaceholdersFromTemplateJson
};