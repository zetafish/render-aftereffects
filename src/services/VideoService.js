let ffmpeg = require('fluent-ffmpeg');

/**
 * Reading the generated video details
 * @param {string} filename The video's filename
 * @returns object with the settings | Error
 */
const getFinalVideoDetails = async (filename) => {
    return new Promise((resolve, reject) => {
        try {

            let result = {
                duration: 0,
                bitrate: 0,
                width: 0,
                height: 0
            }

            ffmpeg.setFfprobePath("./ffprobe.exe");
            ffmpeg.ffprobe(filename, function(err, metadata) {
                result.duration = metadata.format.duration;
                result.bitrate = (metadata.format.bit_rate)? Math.round(metadata.format.bit_rate / 1000) : 0;
                metadata.streams.forEach(stream => {
                    if(stream.codec_type === 'video'){
                        result.width = stream.width;
                        result.height = stream.height;
                        resolve(result);
                    }
                });
            });
        } catch (error) {
            reject(new Error(`Failed to get video details: ${error.message}`));
        }
    })
}

/**
 * 
 * @param {string} estimatedLengthFromLog Getting an estimated video length from the output log
 * @param {*} videoDuration The actual video log
 * @returns boolean | error
 */
const compareVideoDuration = async (estimatedLengthFromLog, videoDuration) => {
    return new Promise((resolve, reject) => {
        try {
           
        const seconds = string =>
            string
              .split(":")
              .map((e, i) => (i < 3 ? +e * Math.pow(60, 2 - i) : +e * 10e-6))
              .reduce((acc, val) => acc + val);
        
        const secondsFromLog = seconds(estimatedLengthFromLog);
        const secondsFromVideoDuration = parseInt(videoDuration);
        
        //Usually the log not completely accurate fox example it says: 00:00:15 but the video length is 00:00:14.955
        //So we let one second buffer
        if(Math.abs(secondsFromLog - secondsFromVideoDuration) > 1){
            resolve(false);
        }
        resolve(true);

        } catch (error) {
            reject(new Error(`Failed to compare video duration: ${error.message}`));
        }
    })
}

module.exports = {
    getFinalVideoDetails,
    compareVideoDuration
}