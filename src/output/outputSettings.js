const fs = require('fs');

/**
 * 
 * @param {object} encoding This object contains the settings about the output video
 * @returns 
 */
const outputSettings = (encoding) => {

    let command = {};
    let rawdata = fs.readFileSync('./src/output/config.json');
    let config = JSON.parse(rawdata);

    const extension = findVideoType(encoding.video, config);
    command = generateCommandObject(encoding, config[extension]);
    command = handlingBitrate(command, encoding);

    return {
        extension: extension,
        filename: `output.${extension}`,
        command: command
    };

    /**
     * This function is creating the ffmpeg's command object for the renderer
     * @param {object} encoding The object contains the settings about the output video
     * @param {object} config The config object which checkes if the settings are matched
     * @returns 
     */
    function generateCommandObject(encoding, config){

        let types = ['video', 'audio'];
        let command = {}

        types.forEach(type => {
            for (const key in config[type]) {

                let settings = config[type][key];
                let requested = encoding[type];

                //Check if the incomming setup contains the parameters
                if(requested[key]){
                    if(settings.values && !settings.values.includes(requested[key].toLowerCase())){
                        throw new Error(`The incoming ${key} (${requested[key]}), not supported. Supported values: ${settings.values.join(', ')}`);
                    }
                    command[settings.command] = requested[key];
                    continue;
                }

                //We check that the parameter is required for the container
                if(settings.required){
                    command[settings.command] = settings.default;
                }
            }
        });

        return command;
    }

    /**
     * There are some specific values which have to be converted
     * @param {object} command Object contains ffmpeg commad
     * @param {object} encoding The incoming json object
     */
    function handlingBitrate(command, encoding){

        command['-b:v'] = '16000k'
        if(encoding.video['target-bitrate']){
            command['-b:v'] = (encoding.video['target-bitrate'] * 1000) + 'k';
        }

        command['-maxrate'] = '16000k'
        if(encoding.video['maximum-bitrate']){
            command['-maxrate'] = (encoding.video['maximum-bitrate'] * 1000) + 'k';
        }

        if(encoding.video.bitrate === 'vbr'){
            command['-minrate'] = '16000k'
            if(encoding.video['minimum-bitrate']){
                command['-minrate'] = (encoding.video['minimum-bitrate'] * 1000) + 'k';
            }
        }

        if(encoding.audio.bitrate){
            command['-b:a'] = encoding.audio.bitrate + 'k';
        }else{
            command['-b:a'] = '320k';
        }

        return command;
    }

    /**
     * Checking the output video format
     * @param {object} video The incoming video object
     * @param {object} outputConfig The config
     */
    function findVideoType(video, config){

        if(video.codec){
            for (const key in config) {
                if (Object.hasOwnProperty.call(config, key)) {
                    const element = config[key];

                    if(element.video.codec.values.includes(video.codec)){
                        return key;
                    }
                    
                }
            }

            throw new Error(`Not supported codec format: ${video.codec}`);
        }

        if(video.container){
            for (const key in config) {
                if (Object.hasOwnProperty.call(config, key)) {
                    const element = config[key];

                    if(element.video.container.values.includes(video.container)){
                        return key;
                    }
                    
                }
            }

            throw new Error(`Not supported container format: ${video.container}`);
        }

        throw new Error(`Not recognized output format`);

    }
}


module.exports = {
    outputSettings
};