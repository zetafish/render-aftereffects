const Template = function(baseUrl){

    let templateObj = {
        src: baseUrl,
        composition: 'main',
        settingsTemplate: 'DRAFT'
    }

    return templateObj;
}

module.exports = {
    Template
  };