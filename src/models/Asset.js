//The Asset object
const Asset = (asset, key, placeholder) => {

    if(typeof asset === 'undefined' || !asset.type || typeof asset.type === 'undefined'){
       return `Asset: [${key}] type attribute is missing from API content`;
    }

    if(asset.type !== 'text' && asset.type !== 'script' && (!asset.value || typeof asset.value === 'undefined' || asset.value === '')){
        return `Asset: [${key}] source is missing`;
    }else if(asset.type === 'text' && typeof asset.value === 'undefined'){
        return `Asset: [${key}] text value is missing`;
    }else if(asset.type === 'script' && typeof asset.src === 'undefined'){
        return `Asset: [${key}] src value is missing`;
    }

    //Base Asset object
    let assetObj = {
        composition: '*',
        placeholderName: placeholder,
        layerName: key,
        type: asset.type === 'text' ? 'data' : asset.type
    };

    // If video, audio, image then add an src attribute
    let assetTypes = ['video', 'audio', 'image'];
    if(assetTypes.includes(asset.type)) {
        assetObj.src = asset.value;
    }else {
        assetObj.property = 'Source Text';
        assetObj.value = asset.value;
    }
    return assetObj;
}

module.exports = {
    Asset
};