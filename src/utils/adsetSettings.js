const fs = require('fs');

/**
 * The currently defined specific settings by the adset ids
 * 
 * @type {array}
 * @todo Add it to specific config file
 */
const specificSettingsByAdsetIds = [13126, 13548];

/**
 * Check if an adset id has defined specified settings
 * 
 * @param {number} adsetId 
 * @returns {bool}
 */
const hasSpecificSettings = (adsetId) => {
    return (specificSettingsByAdsetIds.includes(adsetId));
}

/**
 * Return the defined specified settings for an adset id if they exist or null
 * 
 * @param {number} adsetId 
 */
const getSpecificSettings = (adsetId) => {

    if (!hasSpecificSettings(adsetId)) {
        return null;
    }

    var settingsPath = `src/output/specificSettings/${adsetId}.json`;
    if (!fs.existsSync(settingsPath)) {
        throw new Error(`The specific settings for the ${adsetId} adset id were not found!`);
    }

    return JSON.parse(fs.readFileSync(settingsPath, 'utf8'));
}

module.exports = {
    hasSpecificSettings,
    getSpecificSettings
};
