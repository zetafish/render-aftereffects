const addTimeout = (promise, ms) =>
  // eslint-disable-next-line no-async-promise-executor
  new Promise(async (resolve, reject) => {
    const timeout = setTimeout(() => reject(new Error('Timed out')), ms);

    try {
      const result = await promise;
      clearTimeout(timeout);
      resolve(result);
    } catch (err) {
      clearTimeout(timeout);
      reject(err);
    }
  });

module.exports = {
  addTimeout,
};
