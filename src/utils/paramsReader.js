//Checking the incoming parameters
/**
 * 
 * @param {object} details details of the job
 */
const getBaseUrl = (details) => {

    if(typeof details.input.parameters['base-url'] === 'undefined'){
        throw new Error('BaseUrl is missing');
    }

    return details.input.parameters['base-url'];
}

/**
 * 
 * @param {object} details details of the job
 */
const getAdvertiserId = (details) => {

    if(typeof details.meta['advertiser-id'] === 'undefined'){
        throw new Error('AdvertiserId is missing');
    }

    return details.meta['advertiser-id'];
}

/**
 * Get the adset id from the passed job's details
 * 
 * @param {*} details The job's details object
 * 
 * @returns {number}
 */
const getAdsetId = (details) => {

    if(typeof details.meta['adset-id'] === 'undefined'){
        throw new Error('The adsetId is missing');
    }

    return details.meta['adset-id'];
}

/**
 * 
 * @param {object} details details of the job
 */
const getContent = (details) => {

    if(typeof details.input.content === 'undefined'){
        throw new Error('Placeholders are missing');
    }

    if(Object.keys(details.input.content).length === 0){
        throw new Error('Placeholders object is empty');
    }

    return details.input.content;
}

const checkOutputParams = (details) => {

    if(typeof details.encoding.video === 'undefined'){
        throw new Error('Output parameters are missing');
    }

    if(typeof details.encoding.video.codec === 'undefined' && typeof details.encoding.video.extension === 'undefined'){
        throw new Error('Codec and container are missing');
    }

    return details.encoding;
}

module.exports = {
    getBaseUrl,
    getAdvertiserId,
    getAdsetId,
    getContent,
    checkOutputParams
};