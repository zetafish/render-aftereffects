const errorMap = require('../../error-mapping.json');

const fatalMap = errorMap.filter(({ type }) => type === 'FATAL');

/**
 * Get state if given string contains FATAL error
 *
 * @param {string} msg
 */
module.exports = msg => {
  const lines = msg.split(/(\r\n|\n|\r)/gm);

  return lines.some(line => {
    return fatalMap.find(({ test: pattern }) => new RegExp(pattern).test(line.trim()));
  });
};
