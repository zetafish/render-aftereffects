# lemonpi-render-aftereffects

Adobe After Effects worker for render-api

## Add/update the base image

Create the windows base image on your laptop and tag it with `registry.gitlab.com/greenhousegroup/lemonpi/lemonpi-render-aftereffects/base:<VERSION>`.

Obtain an access token from gitlab so that you are able to push docker images to this repo. Go to `User Settings > Personal Access Tokens`. Add a personal access token with permissions `read_registry` and `write_registry`. Take note of the generated access token.

Login to gitlab with the personal access token

```
$ docker logout registry.gitlab.com
Removing login credentials for registry.gitlab.com
$ docker login registry.gitlab.com
Username:
Password:
```

Push the image to the registry

```
docker push registry.gitlab.com/greenhousegroup/lemonpi/lemonpi-render-aftereffects/base:<VERSION>
```

## Using the base image

Refer to the base image in you Dockerfile

```
# Dockerfile
FROM registry.gitlab.com/greenhousegroup/lemonpi/lemonpi-render-aftereffects/base:<VERSION>
...
```

## Building the app image

The app image is created automatically via the pipeline. To run a gitlab job on a shared windows runner on gitlab the following tags have been added to the jobs:

```
tags:
- shared-windows
- windows
- windows-1809
```

See: https://about.gitlab.com/blog/2020/01/21/windows-shared-runner-beta/

*NB* We only have a limited amount of build minutes on the shared runners so it may be necessary to set up self managed runners. In that case the tags have to change to select the self managed runners.
