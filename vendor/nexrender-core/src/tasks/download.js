/* !! DO NOT UPDATE THIS FILE !! */
const fs       = require('fs')
const url      = require('url')
const path     = require('path')
const fetch    = require('node-fetch')
const uri2path = require('file-uri-to-path')
const data2buf = require('data-uri-to-buffer')
const {expandEnvironmentVariables} = require('../helpers/path')
const Downloader = require('nodejs-file-downloader');

// TODO: redeuce dep size
const requireg = require('requireg')
const { resolve } = require('requireg')

const download = async (job, settings, asset) => {

    if (asset.type == 'data') return Promise.resolve();

    const uri = global.URL ? new URL(asset.src) : url.parse(asset.src)
    const protocol = uri.protocol.replace(/\:$/, '');
    let destName = '';

    /* if asset doesnt have a file name, make up a random one */
    if (protocol === 'data' && !asset.layerName) {
        destName = Math.random().toString(36).substring(2);
    } else {
        destName = path.basename(asset.src)
        destName = destName.indexOf('?') !== -1 ? destName.slice(0, destName.indexOf('?')) : destName;
        /* ^ remove possible query search string params ^ */;

        /* prevent same name file collisions */
        if (fs.existsSync(path.join(job.workpath, destName))) {
            destName = Math.random().toString(36).substring(2) + '.' + path.extname(asset.src);
        }
    }

    if (asset.extension) {
        destName += '.' + asset.extension
    }

    asset.dest = path.join(job.workpath, destName);

    switch (protocol) {
        /* built-in handlers */
        case 'data':
            return new Promise((resolve, reject) => {
                try {
                    fs.writeFile(
                        asset.dest, data2buf(asset.src),
                        err => err ? reject(err) : resolve()
                    );
                } catch (err) {
                    reject(err)
                }
            });
            break;

        case 'http':
        case 'https':
            /* !!DO NOT UPDATE THIS FUNCTION FROM nexrender, it uses custom downloader !! */
            return await new Promise((resolve, reject) => {
                const downloader = new Downloader({
                    url: asset.src, 
                    directory: path.dirname(asset.dest), //This folder will be created, if it doesn't exist.
                    maxAttempts: 3,
                    onBeforeSave: async (deducedName) => {
                        /**
                         * Reading the filename from the headers
                         * If return a string here, it will be used as the name(that includes the extension!).
                         */
                        if(typeof deducedName !== 'undefined' && deducedName !== ''){
                            asset.dest = path.join(job.workpath, deducedName);
                            console.log(`The file name is: ${deducedName}`)
                            return deducedName;
                        }

                        /**
                         * If there s3 signed url, and not found the filename in the header search the file name from the url
                         */
                        if(asset.src.indexOf('AWSAccessKeyId') > -1 && asset.src.indexOf('filename') > -1){
                            const regex = 'filename(=|\%3D)(\"|\%22)(.*?)(\"|\%22)';
                            const found = (asset.src).match(regex);
                            if(typeof found[3] !== 'undefined'){
                                asset.dest = path.join(job.workpath, found[3]);
                                return found[3];
                            }
                        }
                        console.log(`FileName not found in the headers keep the original one : ${path.basename(asset.dest)}`)
                        return path.basename(asset.dest);
                    }    
                });
                
                try {
                    downloader.download()
                        .then( res => {
                            settings.logger.log(`[${job.uid}] Assets [${asset.src}] download finished...`)
                            resolve();
                        }) //returns a promise.
                    
                } catch (error) {//IMPORTANT: Handle a possible error. An error is thrown in case of network errors, or status codes of 400 and above.
                    //Note that if the maxAttempts is set to higher than 1, the error is thrown only if all attempts fail.
                    new Error(`${asset.src} has an error during download: ${error.message}`);
                    reject();
                }

            });
            break;

        case 'file':
            /* plain asset stream copy */
            const rd = fs.createReadStream(uri2path(expandEnvironmentVariables(asset.src)))
            const wr = fs.createWriteStream(asset.dest)

            return new Promise(function(resolve, reject) {
                rd.on('error', reject)
                wr.on('error', reject)
                wr.on('finish', resolve)
                rd.pipe(wr);
            }).catch(function(error) {
                rd.destroy()
                wr.end()
                throw error
            })
            break;

        /* custom/external handlers */
        default:

            try {
                return requireg('@nexrender/provider-' + protocol).download(job, settings, asset.src, asset.dest, asset.params || {});
            } catch (e) {
                if (e.message.indexOf('Could not require module') !== -1) {
                    return Promise.reject(new Error(`Couldn\'t find module @nexrender/provider-${protocol}, Unknown protocol provided.`))
                }

                throw e;
            }

            break;
    }
}

/**
 * This task is used to download/copy every entry in the "job.assets"
 * and place it nearby the project asset
 */
module.exports = async function(job, settings) {

    settings.logger.log(`[${job.uid}] downloading assets...`)
    const promises = [].concat(
        download(job, settings, job.template),
        job.assets.map(async (asset) => {
            await download(job, settings, asset)
        })
    )

    return Promise.all(promises).then(_ => job);
}
