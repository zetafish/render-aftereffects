require('dotenv').config();
const { downloadFilesAndInstallFonts, readComposition } = require('../src/services/FileService');
const { basename } = require('path');

async function correctFileTest(){

    //The correct url, where everything is set.
    const baseUrl = 'https://aerenderer-storage.s3.eu-central-1.amazonaws.com/root/175';
    /**
     * Teszting the file donwloands
     */
    const templateFile = await downloadFilesAndInstallFonts(baseUrl);
    console.log(templateFile);
    if(basename(templateFile) === 'project.aep'){
        console.log('Template file found')
    }   
}

/**
 * Testing if the given url isn't correct s3 url
 */
async function wrongBaseUrleTest(){
    
    try {
        //The correct url, where everything is set.
        const baseUrl = 'http://google.com';
        const templateFile = await downloadFilesAndInstallFonts(baseUrl);
        console.log(templateFile);
        if(basename(templateFile) === 'project.aep'){
            console.log('Template file found')
        }      
    } catch (error) {
        console.log(error.message)
    }
    
}

/**
 * Testing if the project files are not set
 */
async function NoProjectFileTest(){

    try {
        //The correct url, where everything is set.
        const baseUrl = 'https://aerenderer-storage.s3.eu-central-1.amazonaws.com/root/empty-folder/'
        /**
         * Teszting the file donwloands
         */
        const templateFile = await downloadFilesAndInstallFonts(baseUrl);
        console.log(templateFile);
        if(basename(templateFile) === 'project.aep'){
            console.log('Template file found')
        }
    } catch (error) {
        console.log('asd')
        console.log(error.message)
    }
}

async function readCompositionTest(){
    try {
        const baseUrl = 'https://aerenderer-storage.s3.eu-central-1.amazonaws.com/FordDebug2';
        const templateFile = await downloadFilesAndInstallFonts(baseUrl);
        console.log(templateFile);
        if(basename(templateFile) === 'project.aep'){
            console.log('Template file found')
        }   

        const composition = await readComposition();
        console.log(composition);
    } catch (error) {
        console.log('asd')
        console.log(error.message)
    }

}
//correctFileTest();
//wrongBaseUrleTest();
//NoProjectFileTest();
readCompositionTest()
