/**
 * Asset and the job obejct test
 */

const { Asset } = require('../src/models/Asset');
const {readFileSync} = require('fs');
const details =  JSON.parse(readFileSync('test/AssetRaw.json', 'utf8'));

let content = details.input.content;
let assets = [];
let job = {};

job.id = details['job-id'];
job.filename = `${details['correlation-id']}.mp4`;

//hardcoded, quality of the video, should read from the details object or envirement variables
job.type = 'MAIN';

for(const key in content) {
  let assetObj = Asset(content[key], key);
  assets.push(assetObj);
}
job.assets = assets;

console.log(job);