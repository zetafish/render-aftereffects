
const find = require('find');
const { readFileSync } = require('fs');
const { Asset } = require('../src/models/Asset');
/**
 * Reads the layers from template file
 */
 const readPlaceholdersFromTemplateJson = async () => {
    return new Promise((resolve, reject) => {
      find.file('template.json', 'C:\\xampp\\htdocs\\lemonpi-render-aftereffects\\test', files => {
        
        if (!files.length) reject(new Error(`Can't find template.json.`));
        let template = JSON.parse(readFileSync(files[0], 'utf8'));
  
        if(typeof template.placeholders === 'undefined'){
          reject('No placeholders found');
        }
  
        resolve(template.placeholders);
      })
      .error(error => {
        reject(new Error(`Error happent during project.json search: ${error.message}`));
      });
    })
  };

async function run(){
    const placeholders = JSON.parse(readFileSync('C:\\xampp\\htdocs\\lemonpi-render-aftereffects\\test\\data.json', 'utf8'));
    //console.log(placeholders)
    const placeholdersMappedToLayers = await readPlaceholdersFromTemplateJson();

    console.log(placeholdersMappedToLayers)
    assets = []
    for(let placeholder in placeholdersMappedToLayers){
        if(typeof placeholdersMappedToLayers[placeholder].layers !== 'undefined'){
            const layers = placeholdersMappedToLayers[placeholder].layers;
            console.log(layers);
            layers.forEach(layer => {
                let assetObj = Asset(placeholders.input.content[placeholder], layer);
                assets.push(assetObj);
            });
        }
    }
}
//Whats happen if a layer is connected a placeholder but a placeholders is not in the list

//Mi van akkor ha van template.json de abban nincs minden olyan placeholder benne amit az API átküld
//Ebben az esetben csak a template.json ben levő layereket használjuk vagy mergeljük_?
run();