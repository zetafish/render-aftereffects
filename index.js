// Modules
require('dotenv').config();
const isFatalError = require('./src/utils/fatal-error-detection');
const { downloadFilesAndInstallFonts, readComposition, uploadFinalVideo, readPlaceholdersFromTemplateJson } = require('./src/services/FileService');
const { initRender } = require('./src/services/RenderService');
const { getDetails, reportRunningStatus, sendResult } = require('./src/services/ProjectDetailsService');
const { Asset } = require('./src/models/Asset');
const { getBaseUrl, getAdvertiserId, getAdsetId, getContent, checkOutputParams } = require('./src/utils/paramsReader');
const { hasSpecificSettings, getSpecificSettings } = require('./src/utils/adsetSettings');
const { outputSettings } = require('./src/output/outputSettings');
const { readFileSync } = require('fs');
const { getFinalVideoDetails, compareVideoDuration } = require('./src/services/VideoService');

//Reading enviroment variables
const RENDER_API = process.env.RENDER_API;
const CORRELATION_ID = process.env.CORRELATION_ID;
const BUCKET = process.env.BUCKET;
const REGION = (typeof process.env.REGION !== 'undefined') ? process.env.REGION : 'eu-west-1';
const CWD = process.cwd();
process.env.calculatedDuration = false;
let videoCompareResult = false;

//Logger, error checking
const logger = {
  log: msg => {
    console.log(msg);
    const sanitizedStr = msg.trim().replace(/\n/, '');
    if (isFatalError(sanitizedStr)) {
      throw new Error(sanitizedStr);
    }
  },
};

async function run() {

  try {

    //Gets the project details
    console.log('Reading the project details...');

    // NOTE: Commented for LOCAL testing purposes ONLY in local
    //const details = JSON.parse(readFileSync('test/FordDebug2.json', 'utf8'));

    const details = await getDetails(RENDER_API, CORRELATION_ID);

    //Creating basic variables
    const baseUrl = getBaseUrl(details);
    const advertiserId = getAdvertiserId(details);
    const content = getContent(details);

    //If no encoding set in API
    if (!details.encoding || typeof details.encoding === 'undefined') {

      //If there a special encoding for an adsetId
      const adsetId = getAdsetId(details);
      details.encoding = getSpecificSettings(adsetId);

      if (!details.encoding) {
        //Default setting
        details.encoding = JSON.parse(readFileSync('src/output/defaultSettings.json', 'utf8'));
      }
    }
    console.log(details.encoding);
    //checking the params
    const outputDetails = checkOutputParams(details);

    //Before we start the render check the output settings, will be possible to generate
    const output = outputSettings(outputDetails);

    let job = {};
    let assets = [];
    //hardcoded, quality of the video, should read from the details object or envirement variables
    job.type = 'MAIN';

    //Set up the given asset format
    console.log('Generating asset array...');
    for (const key in content) {
      let assetObj = Asset(content[key], key, key);
      assets.push(assetObj);
    }
    job.assets = assets;

    //Downloading the project materials
    console.log('Downloading files and installing fonts...');
    let templateFilePath = await downloadFilesAndInstallFonts(baseUrl);

    //check template.json
    const templateJsonContent = await readPlaceholdersFromTemplateJson(content);
    if (templateJsonContent) {
      job.assets = templateJsonContent.assets;
      job.composition = templateJsonContent.composition;
      console.log('Using template.json...')
    }else{
      //Reading the composition name
      console.log('Getting the composition name...');
      job.composition = await readComposition();
    }

    //Initializing the renderer
    console.log('Initializing the render...');
    const render = initRender({ logger });

    console.log('Getting running\'s endpoint status...');
    //await reportRunningStatus(RENDER_API, CORRELATION_ID);

    //Starting the rendeer
    console.log('Start the render...');

    buu
    
    await render(templateFilePath, job, CWD, output)

    //Reading info from the generated video
    let finalVideoDetails = await getFinalVideoDetails(output.filename);

    //compare video duration
    videoCompareResult = await compareVideoDuration(process.env.calculatedDuration, finalVideoDetails.duration);

    //If the length is OK, report the result
    if (!videoCompareResult) {
      await render(templateFilePath, job, CWD, output)
    }

    //Reading info from the generated video
    finalVideoDetails = await getFinalVideoDetails(output.filename);

    //Upload the result file to s3
    console.log('Uploading the final video to s3...');
    const finalUrl = await uploadFinalVideo(BUCKET, CORRELATION_ID, advertiserId, output.filename, output.extension);

    console.log('Sending back the success result...');
    await sendResult(RENDER_API, CORRELATION_ID, 'succeeded', finalUrl, finalVideoDetails);

  } catch (error) {

    console.log(error.message)
    await sendResult(RENDER_API, CORRELATION_ID, 'failed', error.message);
  }
}

try {
  run();
} catch (error) {
  console.log(error)
}
